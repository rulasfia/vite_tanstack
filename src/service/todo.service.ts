import { useQuery } from "@tanstack/react-query";

const BASE_URL = "https://jsonplaceholder.typicode.com";

/** Get All Todo */
async function getAllTodo() {
	const res = await fetch(`${BASE_URL}/todos`);
	if (!res.ok) {
		/** customize error here (400, 401, 500, dll) */
		throw new Error("Invalid request!");
	}

	return await res.json();
}

/** Get All Todo Hooks */
export function useGetAllTodo() {
	const q = useQuery({
		/** unique identifier for all todo resource */
		queryKey: ["todos"],
		queryFn: getAllTodo,
	});

	return q;
}
