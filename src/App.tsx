import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { useGetAllTodo } from "./service/todo.service";

function App() {
	/** call custom hooks directly in the component
	 *  get request status (loading, error, dst) and data as a result
	 */
	const { isLoading, isError, data } = useGetAllTodo();

	return (
		<>
			<div>
				<a href="https://vitejs.dev" target="_blank">
					<img src={viteLogo} className="logo" alt="Vite logo" />
				</a>
				<a href="https://react.dev" target="_blank">
					<img src={reactLogo} className="logo react" alt="React logo" />
				</a>
			</div>
			<h1>Vite + React</h1>
			<div>
				{isLoading ? <p>Loading...</p> : null}
				{isError ? <p>Something went wrong...</p> : null}
				{data && data.length > 0
					? data.slice(0, 20).map((item) => <p>{item.title}</p>)
					: null}
			</div>
		</>
	);
}

export default App;
